package handler

import (
	"fmt"
	"log"
	"net/http"
)

// Health returns status code 200 Ok GET and
// 405 if request has wrong method.
func Health(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		w.WriteHeader(http.StatusOK)
		_, err := fmt.Fprintln(w, "Ok")
		if err != nil {
			log.Printf("route.Health error: %v", err)
			return
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		_, err := fmt.Fprintln(w, "Error")
		if err != nil {
			log.Printf("route.Health error: %v", err)
			return
		}
	}
}
