package handler

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/valerybaturin/microservice/internal/service/data"
)

func Ingest(service data.Processor) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			var req data.Request

			// Decode client's request body.
			err := json.NewDecoder(r.Body).Decode(&req)
			if err != nil {
				log.Println("Decode Error")
				w.WriteHeader(http.StatusBadRequest)
				_, _ = w.Write([]byte("Error"))
			}

			// Sender data.Request to the processing service.
			err = service.Process(req)
			if err != nil {
				log.Printf("route.Ingest err: %v", err)
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = w.Write([]byte("Error"))
			}
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
			_, _ = w.Write([]byte("Error"))
		}
	}
}
