package integration

import (
	"errors"
	"os"
)

type Config struct {
	healthURL  string
	serviceURL string
}

func NewConfig() (Config, error) {
	// Check gor QUEUE_HEALTH_URL variable
	healthURL := os.Getenv("QUEUE_HEALTH_URL")
	if healthURL == "" {
		return Config{}, errors.New("QUEUE_HEALTH_URL variable is empty")
	}

	// Check gor QUEUE_URL variable
	serviceURL := os.Getenv("QUEUE_URL")
	if serviceURL == "" {
		return Config{}, errors.New("QUEUE_URL variable is empty")
	}

	servicePort := os.Getenv("QUEUE_PORT")
	if servicePort != "" {
		serviceURL = serviceURL + ":" + servicePort
	}

	return Config{healthURL: healthURL, serviceURL: serviceURL}, nil
}
