package integration

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
)

type Service interface {
	Send(data []byte) error
	CheckHealth()
}

type service struct {
	config Config
}

func NewService(config Config) Service {
	return &service{config: config}
}

func (s *service) Send(data []byte) error {
	// fmt.Println(string(data))
	r := bytes.NewReader(data)
	request, err := http.NewRequest("POST", s.config.serviceURL, r)
	if err != nil {
		return err
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	res, err := client.Do(request)
	if err != nil {
		return err
	}
	defer func(Body io.ReadCloser) {
		err = Body.Close()
		if err != nil {
			return
		}
	}(res.Body)

	if res.StatusCode != http.StatusOK {
		switch res.StatusCode {
		case http.StatusInternalServerError:
			body, err := io.ReadAll(res.Body)
			if err != nil {
				log.Println("cannot read response body")
				return fmt.Errorf("queue status code is 500, error Message: %v", string(body))
			}
		default:
			return fmt.Errorf("queue status code is not 200: %v", res.StatusCode)
		}
	}

	return nil
}
