package integration

import (
	"log"
	"net/http"
	"time"
)

// CheckHealth is a function that checks Queue's health status.
func (s *service) CheckHealth() {
	// Create a http client with timeout of 2 seconds.
	c := http.Client{Timeout: 2 * time.Second}

	// Check for health response in the loop
	// with a delay of 3 seconds.
	for {
		time.Sleep(3 * time.Second)
		resp, err := c.Get(s.config.healthURL)
		if err != nil {
			log.Printf("integration.CheckHealth error: %v", err)
			continue
		}
		if resp.StatusCode != http.StatusOK {
			log.Println("integration.CheckHealth error: status code is not 200")
			continue
		}
	}
}
