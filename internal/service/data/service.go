package data

import (
	"encoding/json"
	"errors"
	"log"
)

type Processor interface {
	Process(request Request) error
}

type Queue interface {
	Send(data []byte) error
}

type service struct {
	queue Queue
}

func NewService(queue Queue) Processor {
	return &service{queue: queue}
}

type Item struct {
	Amount int    `json:"amount"`
	Code   string `json:"code"`
}

type Request struct {
	RequestID string `json:"requestId"`
	Timestamp int64  `json:"timestamp"`
	Data      []Item `json:"data"`
}

type QueueReq struct {
	RequestID string `json:"requestId"`
	Timestamp int64  `json:"timestamp"`
	Amount    int    `json:"amount"`
	Code      string `json:"code"`
}

// Process is a service's method which gets the data from the POST route,
// does payload manipulation and passes it to the Queue microservice.
func (s *service) Process(request Request) error {
	if len(request.Data) > 0 {
		data := make([]QueueReq, len(request.Data))

		// Divide an array from data object into the separate requests.
		for i := 0; i < len(request.Data); i++ {
			data[i] = QueueReq{
				RequestID: request.RequestID,
				Timestamp: request.Timestamp,
				Amount:    request.Data[i].Amount,
				Code:      request.Data[i].Code,
			}
		}

		// Send []QueueReq to the service.
		err := s.sendData(data)
		if err != nil {
			log.Printf("data.Process.sendData error: %v", err)
			return err
		}

		return nil
	}

	return errors.New("data is empty")
}

func (s *service) sendData(data []QueueReq) error {
	for _, v := range data {
		// Marshal to the bytes.
		b, err := json.Marshal(&v)
		if err != nil {
			log.Println("data.Process Marshal Error")
			return err
		}

		// Send it to the queue service.
		err = s.queue.Send(b)
		if err != nil {
			return err
		}
	}

	return nil
}
