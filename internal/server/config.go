package server

import (
	"errors"
	"os"
)

type Config struct {
	port string
}

// NewConfig returns an application configuration or an error.
func NewConfig() (Config, error) {
	p := os.Getenv("APP_PORT")
	if p == "" {
		return Config{}, errors.New("APP_PORT variable is empty")
	}

	return Config{
		port: ":" + p,
	}, nil
}
