package server

import (
	"log"
	"net/http"

	"gitlab.com/valerybaturin/microservice/internal/service/data"
	"gitlab.com/valerybaturin/microservice/internal/transport/rest/handler"
)

// Run starts our http server.
func Run(conf Config, service data.Processor) error {
	// Get server port from config.
	s := &http.Server{
		Addr: conf.port,
	}

	// Handlers
	http.HandleFunc("/health", handler.Health)
	http.HandleFunc("/ingest", handler.Ingest(service))

	// Start server.
	log.Printf("Starting http server on port %v", conf.port)
	err := s.ListenAndServe()
	if err != nil {
		return err
	}

	return nil
}
