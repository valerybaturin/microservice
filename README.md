# Go microservice

## Before you start
This application uses the following env variables:

```dotenv
APP_PORT="" #Application port 
QUEUE_HEALTH_URL="" #Full URL to health endpoint
QUEUE_URL #URL to Queue service
QUEUE_PORT #Queue microservice port. If it's blank, app will use default port 80
```

## Build and run docker image
To build and run docker image you may use this command. Please, don't forget to change your env. variables.

```shell
docker build . -t micro && docker run -e APP_PORT=8080 -e QUEUE_HEALTH_URL=https://google.com -e QUEUE_URL=localhost micro
```

#### Personal achievements:
- No dependencies. Only Go standard library.