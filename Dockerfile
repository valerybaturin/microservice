FROM golang:1.18.1-alpine as builder
# build
WORKDIR /builder
COPY . .
WORKDIR /builder/cmd/server

RUN apk --update add git
RUN CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -o /server

FROM alpine
COPY --from=builder /server /bin/server

ENV APP_PORT="8080"
ENV QUEUE_URL="localhost"
ENV QUEUE_PORT=""
ENV QUEUE_HEALTH_URL="https://google.com"

EXPOSE ${APP_PORT}
EXPOSE ${QUEUE_PORT}

CMD ["/bin/server"]