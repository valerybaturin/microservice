package main

import (
	"log"

	"gitlab.com/valerybaturin/microservice/internal/integration"
	"gitlab.com/valerybaturin/microservice/internal/server"
	"gitlab.com/valerybaturin/microservice/internal/service/data"
)

func main() {
	// Load http server config.
	log.Println("Reading configuration")
	appConfig, err := server.NewConfig()
	if err != nil {
		log.Fatal(err)
	}

	// Load queue config.
	queueConfig, err := integration.NewConfig()
	if err != nil {
		log.Fatal(err)
	}

	// Start new integration service for Queue.
	log.Println("Starting integration service")
	queueService := integration.NewService(queueConfig)

	// Check integration microservice's health.
	go queueService.CheckHealth()

	// Create new processing service.
	log.Println("Starting processing service")
	appService := data.NewService(queueService)

	// Start the server with standard net/http library.
	if err = server.Run(appConfig, appService); err != nil {
		log.Fatal(err)
	}
}
